import logging

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

import settings
from users import Chat
from handlers import GeneralHandler


class HenkBot:
    """
    Telegram bot HenkBot (@SebbaBot).
    """

    # shortcut to the logger defined in settings
    logger = logging.getLogger(settings.BOT_NAME)

    # glabally defined commands which will just result in a simple text reply
    commands = settings.BOT_COMMANDS
    messages = settings.BOT_MESSAGES

    def __init__(self):

        # updater, the actual bot api wrapper.
        self.updater = Updater(token=settings.TOKEN)

        # shortcut for the dispatcher and queue
        self.dispatcher = self.updater.dispatcher
        self.queue = self.updater.job_queue

        # add handlers on init as it's part of the bot itself, not the procedure
        self.add_handlers()

        # this will catch all other interactions with the bot.
        self.dispatcher.add_handler(
            GeneralHandler(self.log_interaction),
        )

    def add_handlers(self):
        """
        This will add all the `simple` commands to the handlers of the dispatcher.
        """

        # each command needs its own command handler
        for command in self.commands:
            self.dispatcher.add_handler(
                CommandHandler(command, self.simple_reply_to_command)
            )

        # all replies to text can be caught by the same handler
        self.dispatcher.add_handler(
            MessageHandler(Filters.text, self.simple_reply_to_text)
        )

        # listener for the help command
        self.dispatcher.add_handler(
            CommandHandler('help', self.help_command)
        )

        # listener for photo uploads
        self.dispatcher.add_handler(
            MessageHandler(Filters.photo, self.reply_to_photo)
        )

    @classmethod
    def log_interaction(cls, bot, update):
        cls.logger.info(f'{update.effective_chat.id} - {update.effective_user.id} - {update.effective_message.text}')
        if update.effective_message.effective_attachment:
            try:
                attachment_type = type(update.effective_message.effective_attachment[-1])
            except KeyError:
                attachment_type = type(update.effective_message.effective_attachment)
            cls.logger.info(f'{update.effective_message.message_id} - {attachment_type}')

    @classmethod
    def reply_to_photo(cls, bot, update):

        cls.log_interaction(bot, update)
        # telegram photos get sent in multiple sizes and thus `photos` is a list of
        # different sizes of the same photo
        photos = update.message.photo

        chat = Chat(update.message.chat_id)

        # take the largest size available
        photo = photos[-1]
        chat.save_photo(photo.get_file())
        bot.send_message(
            chat_id=chat.chat_id,
            text='Thanks for sending the picture.'
        )

    @classmethod
    def simple_reply_to_command(cls, bot, update):
        cls.log_interaction(bot, update)
        command = update.message.text
        check_command = cls.commands.get(command.strip('/'), None)
        if check_command:
            text = check_command[0]
            bot.send_message(
                chat_id=update.message.chat_id,
                text=text,
            )
        else:
            return None

    @classmethod
    def simple_reply_to_text(cls, bot, update):
        cls.log_interaction(bot, update)
        text = update.message.text.lower()
        reply = cls.messages.get(text, None)
        if reply:
            bot.send_message(
                chat_id=update.message.chat_id,
                text=reply,
            )
        else:
            return None

    @classmethod
    def help_command(cls, bot, update):
        cls.log_interaction(bot, update)
        reply = []
        for command, values in cls.commands.items():
            reply.append(f'/{command}: {values[1]}')
        reply = '\n'.join(reply)
        bot.send_message(
            chat_id=update.message.chat_id,
            text=reply,
        )

    def run(self):
        self.updater.start_polling()
