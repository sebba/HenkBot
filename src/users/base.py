import os

import settings


class Chat:
    """
    Class representing someone being talked to.
    """

    def __init__(self, chat_id):
        self.chat_id = chat_id

    def __str__(self):
        return str(self.chat_id)

    @property
    def photo_folder(self):
        """
        Folder in which photos of a user can be stored.
        """
        path = os.path.join(settings.MEDIA_DIR, str(self.chat_id))

        # make sure a path exists for the user
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def save_photo(self, f):
        """
        f: File type from telegram (this is a single size, not a list of multiple)
        """
        f.download(custom_path=os.path.join(self.photo_folder, f.file_id) + '.jpeg')
