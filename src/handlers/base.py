from telegram.ext import Handler

class GeneralHandler(Handler):
    """
    Generic handler used to trigger a handler every time someone messages the bot.
    """

    def check_update(self, update):
        return True

    def handle_update(self, update, dispatcher):
        self.callback(dispatcher.bot, update)
