#!/usr/bin/env python3
"""
Main entry for running the bot.
"""
import argparse
import logging

from bot import HenkBot
import settings

def set_up_logging(log):
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO,
                        filename=log)
    logging.Logger(settings.BOT_NAME)

def run_bot(log):
    if log:
        log = log[0]
    set_up_logging(log)
    log_name = log if log else 'Standard Output'
    print(f'Running bot: {settings.BOT_NAME}\n\
Using log: {log_name}\nMedia path: {settings.MEDIA_DIR}\n')
    HenkBot().run()


# set up argument parser
parser = argparse.ArgumentParser()
parser.add_argument(
    'command',
    help='run: runs the bot.',
    action='store',
)
parser.add_argument(
    '--log',
    help='Log file to be used.',
    action='store',
    nargs=1,
    default=None,
)
args = parser.parse_args()

if 'command' in args:
    if args.command == 'run':
        run_bot(args.log)
    else:
        print('Command not recognized.')
