# HenkBot

## Requirements
* Python>=3.7.0 or Python>=3.6.6
* pip>=18.0
* virtualenv & virtualenvwrapper
* A Telegram token (https://core.telegram.org/bots)

## Description
Currently this is a general purpose Telegram Bot which is easy to add simple replies and commands.

### Features
* Simple text replies to incoming messages.
* Simple text replies to defined commands.
* Saving of incoming photos in user folders.
* Built in logging system.

## Installation
From a bash-like shell:
```
cd ~
mkvirtualenv -p /usr/bin/python3.7 HenkBot (for python3.6 use /usr/bin/python3.6)
workon HenkBot (if virtualenv isn't already active)
git clone https://gitlab.com/sebba/HenkBot.git
cd HenkBot
pip install -r requirements.txt
cd src
cp settings.py.template settings.py
vi/vim/nano/emacs settings.py
```
From here fill in the required settings followed by:
```
./main.py run
```
Your bot should now be running
